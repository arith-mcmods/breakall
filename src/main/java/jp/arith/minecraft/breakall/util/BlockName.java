package jp.arith.minecraft.breakall.util;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;

public class BlockName extends Name {
    public BlockName(String modid, String name, int data) {
        super(modid, name, data);
    }

    public static BlockName tryParse(String name) {
        Name n = Name.tryParse(name);
        if (n != null)
            return new BlockName(n.modid, n.name, n.data);
        return null;
    }

    public static BlockName create(Block block, int meta) {
        Name n = Name.tryParse(Block.blockRegistry.getNameForObject(block));
        if(n != null)
            return new BlockName(n.modid, n.name, meta);
        return null;
    }

    public static BlockName create(Block block) {
        return create(block, -1);
    }

    public boolean verify() {
        return GameRegistry.findBlock(modid, name) != null;
    }
}
