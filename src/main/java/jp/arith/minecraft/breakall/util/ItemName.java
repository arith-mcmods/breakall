package jp.arith.minecraft.breakall.util;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class ItemName extends Name {
    public ItemName(String modid, String name, int data) { super(modid, name, data); }

    public static ItemName tryParse(String name) {
        Name n = Name.tryParse(name);
        if(n != null)
            return new ItemName(n.modid, n.name, n.data);
        return null;
    }


    public static ItemName create(Item item) {
        return create(item, -1);
    }

    public static ItemName create(Item item, int damage) {
        Name n = Name.tryParse(Item.itemRegistry.getNameForObject(item));
        if(n != null)
            return new ItemName(n.modid, n.name, damage);
        return null;
    }

    public static ItemName create(ItemStack itemStack) {
        return create(itemStack.getItem(), itemStack.getItemDamage());
    }

    public boolean verify() {
        return GameRegistry.findItem(modid, name) != null;
    }
}
