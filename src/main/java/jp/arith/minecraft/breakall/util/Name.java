package jp.arith.minecraft.breakall.util;

public class Name {
    public final String modid;
    public final String name;
    public final int data;
    public boolean isDataEnabled() { return data >= 0; }

    protected Name(String modid, String name, int data) { this.modid = modid; this.name = name; this.data = data; }

    protected static Name tryParse(String name) {
        String[] s = name.split(":");
        if(s.length != 1  && s.length != 2)
            return null;

        if(s.length == 1)
            s = new String[] { "minecraft", s[0] };

        String[] t = s[1].split("#");

        if(t.length != 1  && t.length != 2)
            return null;

        int meta = -1;
        if(t.length == 2) {
            try {
                meta = Integer.parseInt(t[1]);
            } catch (NumberFormatException ex) {
                return null;
            }
        }

        return new Name(s[0], t[0], meta);
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof Name && modid.equals(((Name) o).modid) && name.equals(((Name) o).name) && (!isDataEnabled() || !((Name) o).isDataEnabled() || data == ((Name) o).data);
    }
}
