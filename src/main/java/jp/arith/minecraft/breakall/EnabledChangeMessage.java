package jp.arith.minecraft.breakall;

import io.netty.buffer.ByteBuf;
import cpw.mods.fml.common.network.simpleimpl.IMessage;

public class EnabledChangeMessage implements IMessage {
	
	Mode mode = Mode.TOGGLE;
	public Mode getMode() { return mode; }

	public EnabledChangeMessage() { }
	public EnabledChangeMessage(Mode mode) { this.mode = mode; }

	@Override
	public void fromBytes(ByteBuf arg0) {
		mode = Mode.values()[arg0.readInt()];
	}

	@Override
	public void toBytes(ByteBuf arg0) {
		arg0.writeInt(mode.ordinal());
	}
	
	public enum Mode {
		ENABLED, DISABLED, TOGGLE
	}

}
