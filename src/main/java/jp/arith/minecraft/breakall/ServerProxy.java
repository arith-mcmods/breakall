package jp.arith.minecraft.breakall;

import java.util.HashMap;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent;
import cpw.mods.fml.relauncher.Side;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ChatComponentText;
import net.minecraft.world.World;

public class ServerProxy extends CommonProxy {
	
	HashMap<EntityPlayer, Boolean> enabled = new HashMap<EntityPlayer, Boolean>();
	
	public Boolean getEnabled(EntityPlayer player) {
		return enabled.containsKey(player) ? enabled.get(player) : BreakAll.getConfig().getStartMode();
	}

	public void setEnabled(EntityPlayer player, Boolean value) {
		enabled.put(player, value);
	}
	
	@Override
	public void breakAll(EntityPlayer player, ItemStack tool, Block block, int metadata, World w, int x, int y, int z) {
		if(getEnabled(player))
			super.breakAll(player, tool, block, metadata, w, x, y, z);
	}
	
	public void changeEnabled(EntityPlayerMP player, EnabledChangeMessage.Mode mode) {
		switch(mode) {
			case TOGGLE:
				setEnabled(player,! getEnabled(player));
				break;
			case ENABLED:
				setEnabled(player, true);
				break;
			case DISABLED:
				setEnabled(player, false);
				break;
		}
		
		player.addChatMessage(new ChatComponentText("BreakAll: " + (getEnabled(player) ? "Enabled" : "Disabled")));
	}

	@SubscribeEvent
	public void tick(TickEvent.ServerTickEvent e) {
		if(e.side == Side.SERVER)
            BreakAll.getConfig().pollUpdate();
	}
}
