package jp.arith.minecraft.breakall;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import jp.arith.minecraft.breakall.tools.AxeBehavior;
import jp.arith.minecraft.breakall.tools.ExtraToolBehavior;
import jp.arith.minecraft.breakall.tools.PickaxeBehavior;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.world.BlockEvent.BreakEvent;

public abstract class CommonProxy {
	
	public void init() {
		FMLCommonHandler.instance().bus().register(this);
    	MinecraftForge.EVENT_BUS.register(this);

        pickaxe = new PickaxeBehavior();
        axe = new AxeBehavior();
    }

    PickaxeBehavior pickaxe;
    AxeBehavior axe;


    @SubscribeEvent
    public void blockBreaked(BreakEvent event) {
        if(event.getPlayer() != null) {
            if(event.getPlayer().getHeldItem() != null) {
                ItemStack itemStack = event.getPlayer().getHeldItem();

                breakAll(event.getPlayer(), itemStack, event.block, event.blockMetadata, event.world, event.x, event.y, event.z);
            }
        }
    }

	public void breakAll(EntityPlayer player, ItemStack tool, Block block, int metadata, World w, int x, int y, int z) {
        pickaxe.breakAll(player, tool, block, metadata, w, x, y, z);
        axe.breakAll(player, tool, block, metadata, w, x, y, z);

        String[] tools = BreakAll.getConfig().getExtraToolNames();
        for(String t : tools)
            new ExtraToolBehavior(t).breakAll(player, tool, block, metadata, w, x, y, z);

	}
}
