package jp.arith.minecraft.breakall;

import java.util.Random;

import cpw.mods.fml.common.*;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import cpw.mods.fml.relauncher.Side;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


@Mod(modid = BreakAll.MODID, version = BreakAll.VERSION, name = BreakAll.MODNAME)
public class BreakAll
{
    public static final String MODID = "Arith.BreakAll";
    public static final String VERSION = "0.0.8";
    public static final String MODNAME = "BreakAll";

	@SidedProxy(clientSide = "jp.arith.minecraft.breakall.ClientProxy", 
			serverSide = "jp.arith.minecraft.breakall.ServerProxy")
	static CommonProxy proxy;
	public static CommonProxy getProxy() { return proxy; }
	
    public static final SimpleNetworkWrapper CHANNEL = NetworkRegistry.INSTANCE.newSimpleChannel(BreakAll.MODID);
    
    static Config config;
    public static Config getConfig() { return config; } 
	
    public static final Logger LOG = LogManager.getLogger("BreakAll");
    
    int tickCount = 0;
    
    public static final Random RND = new Random();
    
    @EventHandler
    public void init(FMLInitializationEvent event)
    {
    	BreakAll.CHANNEL.registerMessage(EnabledChangeMessageHandler.class, EnabledChangeMessage.class, 0, Side.SERVER);

    	proxy.init();
    }

	@EventHandler
	public void preInit(FMLPreInitializationEvent event)  {
		BreakAll.LOG.debug("Debug output is not suppressed.");
		BreakAll.LOG.trace("Trace output is not suppressed.");
		
		config = Config.Load(event.getSuggestedConfigurationFile());
	}
	
	@EventHandler
	public void postInit(FMLPostInitializationEvent event) { }
}
