package jp.arith.minecraft.breakall;

import jp.arith.minecraft.breakall.util.BlockName;
import jp.arith.minecraft.breakall.util.ItemName;
import net.minecraftforge.common.config.Configuration;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;

public class Config {
    Configuration cfg;

    public boolean getDropExpOrb() { return cfg.get("common", "dropXpOrb", false).getBoolean(); }
    public int getAccelDecayLeaves() { return cfg.get("common", "accelDecayLeaves", 10).getInt(); }
    public boolean getStartMode() { return cfg.get("common", "startMode", true).getBoolean(); }
    public String[] getExtraToolNames() { return cfg.get("common", "extraTools", new String[] { "shovel" }).getStringList(); }
    public ExtraTool getExtraTool(String tool) { return new ExtraTool(cfg, tool); }
    public ExtraTool[] getExtraTools() {
        String[] toolNames = getExtraToolNames();
        ExtraTool[] tools = new ExtraTool[toolNames.length];
        for(int i = 0; i < toolNames.length; i++)
            tools[i] = getExtraTool(toolNames[i]);
        return tools;
    }

    public double getExhaustionRate() { return cfg.get("common", "exhaustionRate", 0.95).getDouble(); }

    public boolean getInterruptWhenToolBroken() { return cfg.get("common", "interruptWhenToolBroken", true).getBoolean(); }

    public String[] getBlockGroupTextList() { return cfg.get("common", "blockGroups",
            new String[] {
                    "grass,dirt,farmland,mycelium",
                    "redstone_ore,lit_redstone_ore"
            }).getStringList(); }

    public String[][] getBlockGroups() {
        String[] groupTextList = getBlockGroupTextList();
        String[][] groups = new String[groupTextList.length][];
        for(int i = 0; i < groupTextList.length; i++)
            groups[i] = groupTextList[i].split(",");
        return groups;
    }

    void reload() {
        cfg.load();

        if(!cfg.hasCategory("extratool_shovel")) {
            cfg.get("extratool_shovel", "searchRadius", 4).set(4);
        }

        getDropExpOrb();
        getAccelDecayLeaves();
        getStartMode();
        getBlockGroupTextList();
        getInterruptWhenToolBroken();
        getExhaustionRate();

        getExtraTools();

        verify();
    }

    static void verifyItemName(String itemName) {
        ItemName in = ItemName.tryParse(itemName);
        if(in == null)
            BreakAll.LOG.error("Config file error: Invalid item name: " + itemName);
        else if(!in.verify())
            BreakAll.LOG.error("Config file error: Invalid item name: " + itemName);
    }

    static void verifyBlockName(String blockName) {
        BlockName bn = BlockName.tryParse(blockName);
        if(bn == null)
            BreakAll.LOG.error("Config file error: Invalid item name: " + blockName);
        else if(!bn.verify())
            BreakAll.LOG.error("Config file error: Invalid item name: " + blockName);
    }

    void verify() {
        for(String[] group : getBlockGroups())
            for(String block : group)
                verifyBlockName(block);

        pickaxe.verify();
        axe.verify();

        for(Tool tool : getExtraTools())
            tool.verify();
    }

    final String filePath;
    final Path path;

    private Config(File file) {
        filePath = file.getPath();
        BreakAll.LOG.info("Config file: " + filePath);

        this.cfg = new Configuration(file);

        pickaxe = new Pickaxe(cfg);
        axe = new Axe(cfg);

        reload();

        save();

        //  コンフィグファイル監視
        this.path = file.toPath();
        Path dirPath = path.getParent();
        FileSystem fs = path.getFileSystem();

        WatchService watcher = null;
        WatchKey watchKey = null;

        try {
            watcher = fs.newWatchService();

            watchKey = dirPath.register(watcher,
                    StandardWatchEventKinds.ENTRY_CREATE,
                    StandardWatchEventKinds.ENTRY_MODIFY,
                    StandardWatchEventKinds.ENTRY_DELETE);


        }catch(IOException e) {
            BreakAll.LOG.error("Failed starting FileWatcher.", e);
        }

        this.watcher = watcher;
        this.watchKey = watchKey;

        Runtime.getRuntime().addShutdownHook(new Shutdown(this));
    }

    final WatchService watcher;
    final WatchKey watchKey;

    public boolean pollUpdate() {
        WatchKey detectedKey = watcher.poll();
        if(detectedKey != null) {
            try {
                if (detectedKey.equals(watchKey)) {
                    BreakAll.LOG.info("Detected config directory updating.");
                    for (WatchEvent<?> e : detectedKey.pollEvents()) {
                        if (e.kind() == StandardWatchEventKinds.OVERFLOW)
                            continue;

                        if (e.context() instanceof Path && path.equals(path.getParent().resolve((Path) e.context()))) {
                            BreakAll.LOG.info("Detected config file updating.");

                            reload();

                            BreakAll.LOG.info("Reloading config file has been completed.");

                            return true;
                        }
                    }
                }
            }finally {
                detectedKey.reset();
            }
        }

        return false;
    }

    class Shutdown extends Thread {
        final Config cfg;

        public Shutdown(Config cfg) {
            super();

            this.cfg = cfg;
        }

        @Override
        public void run() {
            try {
                if(watcher != null)
                    cfg.watcher.close();
            }catch(IOException e) {
                e.printStackTrace(System.err);
            }
            BreakAll.LOG.info("FileWatcher was been closed.");
        }
    }


    public final Pickaxe pickaxe;
    public final Axe axe;

    public class Pickaxe extends Tool {
        public Pickaxe(Configuration cfg) {
            super(cfg, "pickaxe", "pickaxe");
        }

        @Override
        public String[] getDefaultBlockNames() { return new String[] { "obsidian", "glowstone", "lit_redstone_ore" }; }

        @Override
        public boolean getLooseBlockConnection() { return false; }

        @Override
        public String getBlockConnectionStyle() { return "ore"; }
    }

    public class Axe extends Tool {
        Configuration cfg;
        public Axe(Configuration cfg) {
            super(cfg, "axe", "axe");
        }

        @Override
        public String[] getDefaultBlockNames() { return new String[] { "red_mushroom_block", "brown_mushroom_block" }; }

        @Override
        public boolean getLooseBlockConnection() { return true; }

        @Override
        public String getBlockConnectionStyle() { return "tree"; }
    }

    public abstract class Tool {
        protected final Configuration cfg;
        public final String tool;
        protected final String category;

        public Tool(Configuration cfg, String tool, String category) {
            this.cfg = cfg;
            this.tool = tool;
            this.category = category;

            getTools();
            getBlocks();

            getEnableDefaultTools();
            getEnableDefaultBlocks();

            getDigBottom();
            getBlockConnectionStyle();

            getLooseBlockConnection();
            getSearchRadius();
        }

        public void verify() {
            for(String t : getTools())
                verifyItemName(t);

            for(String b : getBlocks())
                verifyBlockName(b);

            String s = cfg.get(category, "blockConnectionStyle", "ore").getString();
            if(!s.equals("ore") && !s.equals("tree")) {
                BreakAll.LOG.error("Config file error: Invalid blockConnectionStyle: \"" + s + "\"");
            }
        }

        protected String[] getDefaultBlockNames() { return new String[0]; }

        public String[] getTools() { return cfg.get(category, "tools", new String[0]).getStringList(); }
        public String[] getBlocks() { return cfg.get(category, "blocks", getDefaultBlockNames()).getStringList(); }

        public boolean getEnableDefaultTools() { return cfg.get(category, "enableDefaultTools", true).getBoolean(); }
        public boolean getEnableDefaultBlocks() { return cfg.get(category, "enableDefaultBlocks", true).getBoolean(); }

        public boolean getDigBottom() { return cfg.get(category, "digBottom", true).getBoolean(); }

        public String getBlockConnectionStyle() {
            String s = cfg.get(category, "blockConnectionStyle", "ore").getString();
            if(!s.equals("ore") && !s.equals("tree"))
                return "ore";
            return s;
        }

        public boolean getLooseBlockConnection() { return cfg.get(category, "looseBlockConnection", false).getBoolean(); }
        public int getSearchRadius() { return cfg.get(category, "searchRadius", 32).getInt(); }
    }

    public class ExtraTool extends Tool {
        public ExtraTool(Configuration cfg, String tool) {
            super(cfg, tool, "extratool_" + tool);
        }
    }

    public void save() { cfg.save(); }
	
	public static Config Load(File file) {
        return new Config(file);
	}
	
}
