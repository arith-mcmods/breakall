package jp.arith.minecraft.breakall;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;

public class EnabledChangeMessageHandler implements IMessageHandler<EnabledChangeMessage, IMessage> {

	@Override
	public IMessage onMessage(EnabledChangeMessage arg0, MessageContext arg1) {
		((ServerProxy)BreakAll.getProxy()).changeEnabled(arg1.getServerHandler().playerEntity, arg0.getMode());
		return null;
	}

}
