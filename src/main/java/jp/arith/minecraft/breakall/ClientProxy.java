package jp.arith.minecraft.breakall;

import cpw.mods.fml.common.gameevent.TickEvent;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ChatComponentText;
import net.minecraft.world.World;

import net.minecraftforge.event.entity.living.LivingEvent;
import org.lwjgl.input.Keyboard;

import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.InputEvent.KeyInputEvent;

public class ClientProxy extends CommonProxy {
	
	Boolean enabled = true;
	public void setEnabled(Boolean value) { enabled = value; }
	public Boolean getEnabled() { return enabled; }
	
	public static final KeyBinding toggleKey = new KeyBinding("arith.breakall.keys.toggle", Keyboard.KEY_G, "arith.breakall.keys.category");
	
	@Override 
	public void init() {
    	ClientRegistry.registerKeyBinding(toggleKey);
    	FMLCommonHandler.instance().bus().register(this);
    	enabled = BreakAll.getConfig().getStartMode();
    	
    	super.init();
	}
	
	@Override
	public void breakAll(EntityPlayer player, ItemStack tool, Block block, int metadata, World w, int x, int y, int z) {
		if(getEnabled() && Minecraft.getMinecraft().isSingleplayer()) {
			super.breakAll(player, tool, block, metadata, w, x, y, z);
		}
	}
	
	@SubscribeEvent
	public void KeyInput(KeyInputEvent event) {
		if(ClientProxy.toggleKey.isPressed()) {
			setEnabled(!getEnabled());
			if(!Minecraft.getMinecraft().isSingleplayer()) {
				BreakAll.CHANNEL.sendToServer(new EnabledChangeMessage(getEnabled() ? EnabledChangeMessage.Mode.ENABLED : EnabledChangeMessage.Mode.DISABLED));
			} else {
				Minecraft.getMinecraft().thePlayer.addChatMessage(new ChatComponentText("BreakAll: " + (getEnabled() ? "Enabled" : "Disabled")));
			}
		}
	}

    @SubscribeEvent
    public void livingUpdate(LivingEvent.LivingUpdateEvent e)
    {
        if(e.entity instanceof EntityPlayer && notifyConfigReloaded) {
            ((EntityPlayer) e.entity).addChatComponentMessage(new ChatComponentText("BreakAll: Reloaded config."));

            notifyConfigReloaded = false;
        }
    }

    private boolean notifyConfigReloaded;

    @SubscribeEvent
    public void tick(TickEvent.ClientTickEvent e) {
        notifyConfigReloaded = notifyConfigReloaded || BreakAll.getConfig().pollUpdate();
    }
}
