package jp.arith.minecraft.breakall.tools;

import jp.arith.minecraft.breakall.BreakAll;
import net.minecraft.block.Block;
import net.minecraft.world.World;

public class AxeBehavior extends ToolBehavior {
    public AxeBehavior() {
        super(BreakAll.getConfig().axe);
    }

    protected boolean matchBlockDefault(Block block, int metadata, World w, int x, int y, int z) {
        return block.isWood(w, x, y, z);
    }
}
