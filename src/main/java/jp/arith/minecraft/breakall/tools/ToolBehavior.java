package jp.arith.minecraft.breakall.tools;

import jp.arith.minecraft.breakall.BreakAll;
import jp.arith.minecraft.breakall.Config;
import jp.arith.minecraft.breakall.util.BlockName;
import jp.arith.minecraft.breakall.util.ItemName;
import net.minecraft.block.Block;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityXPOrb;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

import java.util.ArrayList;
import java.util.List;

public abstract class ToolBehavior {
    public final Config.Tool toolConfig;
    public final String toolType;
    public ToolBehavior(Config.Tool toolConfig) {
        this.toolConfig = toolConfig;
        toolType = toolConfig.tool;
    }

    abstract boolean matchBlockDefault(Block block, int metadata, World w, int x, int y, int z);

    protected boolean matchToolDefault(ItemStack itemStack) {
        return itemStack.getItem().getHarvestLevel(itemStack, toolType) >= 0;
    }

    public boolean matchTool(ItemStack itemStack) {
        if(toolConfig.getEnableDefaultTools() && matchToolDefault(itemStack))
            return true;

        for(String tool : toolConfig.getTools()) {
            ItemName in = ItemName.tryParse(tool);
            if(in != null && in.equals(ItemName.create(itemStack)))
                return true;
        }

        return false;
    }

    public boolean matchBlock(Block block, int metadata, World w, int x, int y, int z) {
        if(toolConfig.getEnableDefaultBlocks() && matchBlockDefault(block, metadata, w, x, y, z))
            return true;

        for(String b : toolConfig.getBlocks()) {
            BlockName bn = BlockName.tryParse(b);
            if(bn != null && bn.equals(BlockName.create(block, metadata)))
                return true;
        }

        return false;
    }


    static boolean matchBlockGroup(BlockName bn1, BlockName bn2, String[][] groups) {
        if(bn1.equals(bn2)) return true;

        for(String[] group : groups) {
            boolean flg1 = false, flg2 = false;
            for(String b : group) {
                BlockName bn = BlockName.tryParse(b);
                if(bn == null) continue;

                flg1 |= bn.equals(bn1);
                flg2 |= bn.equals(bn2);
            }

            if(flg1 && flg2) return true;
        }

        return false;
    }

    public List<BlockLocation> detectBlockConnection(World w, BlockLocation center, Block block, int metadata) {
        List<BlockLocation> queue = new ArrayList<BlockLocation>();
        queue.add(center);

        List<BlockLocation> blockList = new ArrayList<BlockLocation>();
        List<BlockLocation> ignoreList = new ArrayList<BlockLocation>();

        String[][] itemGroups= BreakAll.getConfig().getBlockGroups();

        while(queue.size() > 0) {
            BlockLocation location = queue.get(0);
            queue.remove(0);
            int x = location.x, y = location.y, z = location.z;
            int d = Math.max(Math.abs(x - center.x), Math.max(Math.abs(y - center.y), Math.abs(z - center.z)));

            if (blockList.contains(location))
                continue;

            if(!location.equals(center)) {
                if (w.getBlock(x, y, z) == Blocks.air) continue;

                if (toolConfig.getLooseBlockConnection()) {
                    if(matchBlock(w.getBlock(x, y, z), w.getBlockMetadata(x, y, z), w, x, y, z))
                        blockList.add(location);
                    else {
                        if(!ignoreList.contains(location)) ignoreList.add(location);
                        continue;
                    }
                } else {
                    if (matchBlockGroup(BlockName.create(block, metadata), BlockName.create(w.getBlock(x, y, z), w.getBlockMetadata(x, y, z)), itemGroups))
                        blockList.add(location);
                    else {
                        if(!ignoreList.contains(location)) ignoreList.add(location);
                        continue;
                    }
                }
            }

            if (d < toolConfig.getSearchRadius()) {
                if (toolConfig.getBlockConnectionStyle().equals("ore")) {
                    addIfNotContains(queue, new BlockLocation(x + 1, y, z), ignoreList);
                    addIfNotContains(queue, new BlockLocation(x - 1, y, z), ignoreList);
                    addIfNotContains(queue, new BlockLocation(x, y, z + 1), ignoreList);
                    addIfNotContains(queue, new BlockLocation(x, y, z - 1), ignoreList);
                    addIfNotContains(queue, new BlockLocation(x, y + 1, z), ignoreList);
                    if (toolConfig.getDigBottom())
                        addIfNotContains(queue, new BlockLocation(x, y - 1, z), ignoreList);
                } else {
                    for (int dx = -1; dx <= 1; dx++)
                        for (int dz = -1; dz <= 1; dz++)
                            for (int dy = -1; dy <= 1; dy++) {
                                if (!toolConfig.getDigBottom() && dy < 0) continue;
                                if (dx != 0 || dy != 0 || dz != 0)
                                    addIfNotContains(queue, new BlockLocation(x + dx, y + dy, z + dz), ignoreList);
                            }
                }
            }
        }
        return blockList;
    }
    static <T> void addIfNotContains(List<T> list, T item, List<T> ignore) {
        if(!list.contains(item) && !ignore.contains(item))
            list.add(item);
    }

    class BlockLocation {
        public final int x, y, z;
        public BlockLocation(int x, int y, int z) { this.x = x; this.y = y; this.z = z; }

        @Override
        public boolean equals(Object o) {
            return o != null
                    && o instanceof BlockLocation
                    && x == ((BlockLocation) o).x
                    && y == ((BlockLocation) o).y
                    && z == ((BlockLocation) o).z;
        }
    }

    public void breakAll(EntityPlayer player, ItemStack tool, Block block, int metadata, World w, int x, int y, int z) {
        if(matchTool(tool) && matchBlock(block, metadata, w, x, y, z)) {
            List<BlockLocation> blocks = detectBlockConnection(w, new BlockLocation(x, y, z), block, metadata);

            int cnt = 0;
            double exhaustionRate = BreakAll.getConfig().getExhaustionRate();
            for(BlockLocation b : blocks) {
                if(harvest(player, tool, w, b.x, b.y, b.z, exhaustionRate)) {
                    cnt++;
                    exhaustionRate *= BreakAll.getConfig().getExhaustionRate();
                }
            }
            BreakAll.LOG.trace("tool: " + toolType + " " + (blocks.size() + 1) + "blocks");

            if(!player.capabilities.isCreativeMode && !BreakAll.getConfig().getInterruptWhenToolBroken() && tool.getItem().isDamageable())
                tool.setItemDamage(tool.getItemDamage() + cnt);

            accelDecayLeaves(w, x, y, z);
        }
    }

    static boolean harvest(EntityPlayer player, ItemStack tool, World w, int x, int y, int z, double exhaustionRate) {
        Block b = w.getBlock(x, y, z);
        int m = w.getBlockMetadata(x, y, z);

        if(b.canHarvestBlock(player, m)) {
            if(!player.capabilities.isCreativeMode && BreakAll.getConfig().getInterruptWhenToolBroken() && tool.getItem().isDamageable())
                if(tool.getMaxDamage() > tool.getItemDamage())
                    tool.setItemDamage(tool.getItemDamage() + 1);
                else
                    return false;

            if( BreakAll.getConfig().getDropExpOrb() && !(b.canSilkHarvest(w, player, x, y, z, m) && EnchantmentHelper.getSilkTouchModifier(player)) ) {
                int exp = b.getExpDrop(w, m, EnchantmentHelper.getFortuneModifier(player));
                if(exp > 0) {
                    ArrayList<Entity> entities = new ArrayList<Entity>();
                    entities.add(new EntityXPOrb(w,x,y,z,exp));
                    w.addLoadedEntities(entities);
                }
            }

            if(b.isWood(w, x, y, z))
                accelDecayLeaves(w, x, y, z);

            b.harvestBlock(w, player, x, y, z, m);

            b.breakBlock(w, x, y, z, b, m);
            w.setBlock(x, y, z, Blocks.air);

            return true;
        }
        return false;
    }

    static void accelDecayLeaves(World w, int xx, int yy, int zz) {
        if(BreakAll.getConfig().getAccelDecayLeaves() > 0) {
            for(int x = xx - 4; x <= xx + 4; x ++)
                for(int y = yy - 4; y <= yy + 4; y ++)
                    for(int z = zz - 4; z <= zz + 4; z ++) {
                        Block b = w.getBlock(x, y, z);
                        if(b.isLeaves(w, x, y, z))
                            w.scheduleBlockUpdate(x, y, z, b,
                                    BreakAll.RND.nextInt(BreakAll.getConfig().getAccelDecayLeaves()) + 1);
                    }
        }
    }
}
