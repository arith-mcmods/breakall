package jp.arith.minecraft.breakall.tools;

import jp.arith.minecraft.breakall.BreakAll;
import net.minecraft.block.Block;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraftforge.oredict.OreDictionary;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PickaxeBehavior extends ToolBehavior {
    public PickaxeBehavior() {
        super(BreakAll.getConfig().pickaxe);
    }

    static final Pattern REGEX_ORE = Pattern.compile("^ore[A-Z]");

    protected boolean matchBlockDefault(Block block, int metadata, World w, int x, int y, int z) {
        for (int oreId : OreDictionary.getOreIDs(new ItemStack(block, 1, metadata))) {
            Matcher matcher = REGEX_ORE.matcher(OreDictionary.getOreName(oreId));
            if (matcher.find()) return true;
        }
        return false;
    }
}
