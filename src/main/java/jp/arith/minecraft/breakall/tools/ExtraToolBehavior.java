package jp.arith.minecraft.breakall.tools;

import jp.arith.minecraft.breakall.BreakAll;
import net.minecraft.block.Block;
import net.minecraft.world.World;

public class ExtraToolBehavior extends ToolBehavior {
    public ExtraToolBehavior(String tool) {
        super(BreakAll.getConfig().getExtraTool(tool));
    }

    protected boolean matchBlockDefault(Block block, int metadata, World w, int x, int y, int z) {
        return toolConfig.tool.equals(block.getHarvestTool(metadata));
    }
}
