BreakAll
==============

Yet Another MineAll/CutAll

Modding の練習用に作りました。
某有名一括破壊系 Mod の劣化版みたいな感じです。


動作環境
--------------

*   Minecraft 1.7.10
*   Forge 10.13.4.1558


使い方
--------------

鉱石をツルハシで叩くと周りにある同種の鉱石も壊れます。
木を斧で叩くと周りにある木も壊れます。木の場合は、どんな木でも繋がっていれば壊れます。

デフォルトで [G] キーを押すと ON/OFF が切り替えられます。
キーコンフィグから変更可能です。


なにかあったら
--------------

*   http://forum.minecraftuser.jp/memberlist.php?mode=viewprofile&u=85675

プルリクください Issue 書いてください

*   https://bitbucket.org/arith-mcmods/breakall
